Gem::Specification.new do |s|
    s.name        = 'hex_generator'
    s.version     = '0.4.0'
    s.files       = ["lib/hex_generator.rb"]
    s.authors     = ["jdj"]
    s.summary     = "Hex generation per the d30 sandbox"
  end
