module HexContentGenerator
    SETTLEMENT = {
        :government=> {
            1=>'anarchy',
            2=>'argentocracy',
            3=>'aristarchy',
            4=>'atristocracy',
            5=>'autocracy',
            6=>'cryptocracy',
            7=>'democracy',
            8=>'demonocracy',
            9=>'ecclesiary',
            10=>'ethocracy',
            11=>'gerontocracy',
            12=>'gynocracy',
            13=>'heroarchy',
            14=>'hetarchy',
            15=>'matriarchy',
            16=>'militocracy',
            17=>'monarchy',
            18=>'oligarchy',
            19=>'patriarchy',
            20=>'pedantocracy',
            21=>'pedocracy',
            22=>'phallocracy',
            23=>'plutocracy',
            24=>'prophetocracy',
            25=>'statocracy',
            26=>'statocracy',
            27=>'thearchy',
            28=>'theocracy',
            29=>'tritheocracy',
            30=>'xenocracy'
        }
    }

    def self.settlement_population(settlement_type)
        if settlement_type == 'single dwelling'
            return (HexContentGenerator.die(30)/2).ceil
        end

        if settlement_type == 'thorp'
            return 5 + HexContentGenerator.die(30)
        end

        if settlement_type == 'hamlet'
            return 30 + HexContentGenerator.die(30)
        end

        if settlement_type == 'village'
            return (5+HexContentGenerator.die(30)) * 5
        end

        if settlement_type == 'town, small'
            return (2+HexContentGenerator.die(30)) * 60
        end

        if settlement_type == 'town, large'
            return (2+HexContentGenerator.die(30)) * 100
        end

        if settlement_type == 'city, small'
            return 5000 + (HexContentGenerator.die(30)*200)
        end

        if settlement_type == 'city, large'
            10000 + (HexContentGenerator.die(30)*400)
        end

        return 0
    end
    
    def self.generate_settlement(population_density)
        result = {}

        pop_roll = HexContentGenerator.die(3)
        
        if pop_roll == 1
            population_type = IPD[population_density][HexContentGenerator.die(30)]
            
            if population_type != 'uninhabited'
                result[:total] = HexContentGenerator.settlement_population(population_type)
            end

            if result[:total] && result[:total] > 0
                government_roll = HexContentGenerator.die(30)
                result[:government] = SETTLEMENT[:government][government_roll]
            end

            result[:type] = population_type
        end

        
        result
    end
    
end