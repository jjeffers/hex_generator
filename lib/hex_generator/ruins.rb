module HexContentGenerator
    RUINS = {
        'type' =>{
            1=>'hovel',
            2=>'villa',
            3=>'tower',
            4=>'altar',
            5=>'tomb',
            6=>'crpyt',
            7=>'bunker',
            8=>'hamlet',
            9=>'small castle',
            10=>'small city',
            11=>'hut',
            12=>'manor',
            13=>'keep',
            14=>'shrine',
            15=>'vault',
            16=>'catacombs',
            17=>'blockhouse',
            18=>'village',
            19=>'medium castle',
            20=>'medium city',
            21=>'cottage',
            22=>'palace',
            23=>'citadel',
            24=>'temple',
            25=>'mausoleum',
            26=>'sewer',
            27=>'garrison',
            28=>'town',
            29=>'largse castle',
            30=>'large city'
        },
        'decay-character'=>{
            1=>'burned/charred',
            2=>'collapsed/crumbling',
            3=>'covered in vines/sand/rocks',
            4=>'disfigured/vandalized',
            5=>'moldy/contaminated',
            6=>'sunken',
            7=>'burned/charred & disfigured/destroyed',
            8=>'covered in vines/sand/rocks & moldy/contaminated',
            9=>'sunken & covered with vines/sand/rocks',
            10=>'sunken & collapsed/crumbling & covered'
        },
        'decay-degree' =>{
            1=>'slightly/barely',
            2=>'moderately/noticeably',
            3=>'severely/extremely'
        },
        'inhabitants' =>{
            1=>'chimeras',
            2=>'demi-humans',
            3=>'humanoids',
            4=>'giants',
            5=>'humans',
            6=>'insects',
            7=>'lycanthropes',
            8=>'magical',
            9=>'mammals',
            10=>'molds/slimes/jellies',
            11=>'reptiles/reptillians',
            12=>'undead'
        },
        'size' =>{
            1=>'nuisance',
            2=>'infested',
            3=>'overrun'
        }
    }
end