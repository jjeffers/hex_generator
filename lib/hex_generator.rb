require 'hex_generator/initial_population_density'
require 'hex_generator/natural_features'
require 'hex_generator/ruins'
require 'hex_generator/special_habitation'
require 'hex_generator/settlement'
require 'json'

module HexContentGenerator
    TERRAIN_TYPES = [
        'hills',
        'mountains',
        'forest',
        'plains',
        'swamp',
        'desert',
        'coast'
    ]

    def self.hex_generator(terrain_type, population_density, number_of_hexes, seed = nil)

        seed = srand unless seed
    
        results = ""
        population_densities = IPD.keys()
        terrain_types = TERRAIN_TYPES

        unless population_densities.include?(population_density)
            raise ArgumentError.new "Population density must be one of #{population_densities.join(', ')}"
        end

        unless terrain_types.include?(terrain_type)
            raise ArgumentError.new "Terrain must be one of #{terrain_types.join(', ')}"
        end

        if number_of_hexes < 1 || !(number_of_hexes.is_a? Integer)
            raise ArgumentError.new "Number of hexes must a number > 0"
        end

        results = {}

        (1..(number_of_hexes)).each do |hex_number|
            msg = terrain_type

            feature_data = HexContentGenerator.generate_features(terrain_type, NATURAL_FEATURES[terrain_type])
            population_data = HexContentGenerator.generate_population(population_density)
            
            msg = feature_data[:type]

            results[hex_number] = { type: feature_data[:type] }

            if feature_data[:lair]
                msg += ", lair"
                results[hex_number][:lair] = true
            end

            if population_data[:type] != 'uninhabited'
                results[hex_number][:population_type] = population_data[:type]

                if population_data[:total] > 0
                    results[hex_number][:population_total] = population_data[:total]
                end
            end

            if population_data[:type] == 'ruins'
                results[hex_number].merge!(HexContentGenerator.generate_ruins())
            end

            if population_data[:type] == 'special'
                results[hex_number].merge!(
                    HexContentGenerator.generate_special(terrain_type, population_density))
            end
        end
        
        return results
    end

    def self.die(sides)
        return rand(1..sides)
    end

    def self.generate_features(terrain_type, features_data)
        feature_result = { 'type': terrain_type, 'lair': false }

        if features_data['feature-chance'] >= HexContentGenerator.die(30)
            feature_selection_roll = HexContentGenerator.die(30)
            feature_data = features_data['features'][feature_selection_roll]
            feature_result[:type] = feature_data[0]
            
            lair_percent = feature_data[1]
            if HexContentGenerator.die(100) <= lair_percent
                feature_result[:lair] = true
            end
        end

        return feature_result
    end

    def self.generate_population(population_density)
        population_result = { 'type': 'uninhabited', 'total': 0 }

        pop_roll = HexContentGenerator.die(3)
        if pop_roll == 1
            settlement_result = HexContentGenerator.generate_settlement(population_density)
            population_result.merge!(settlement_result)
        end
        population_result
    end

    def self.generate_ruins()
        return {
            ruin: {
                type: HexContentGenerator::RUINS['type'][HexContentGenerator.die(30)],
                character: HexContentGenerator::RUINS['decay-character'][HexContentGenerator.die(10)],
                degree: HexContentGenerator::RUINS['decay-degree'][HexContentGenerator.die(3)],
                inhabitants: HexContentGenerator::RUINS['inhabitants'][HexContentGenerator.die(12)],
                size: HexContentGenerator::RUINS['size'][HexContentGenerator.die(3)]
            }
        }
    end

    def self.generate_special(terrain_type, population_density)
        if HexContentGenerator.die(2) == 1
            result =SPECIAL_HABITATION_BY_TERRAIN[terrain_type]
        else
            result = SPECIAL_HABITATION_BY_POPULATION_DENSITY[population_density][HexContentGenerator.die(30)]

            if result == 'reroll on Desolate'
                result = SPECIAL_HABITATION_BY_POPULATION_DENSITY['desolate'][HexContentGenerator.die(30)]
            end
        end

        { special: result }
    end

        
end
