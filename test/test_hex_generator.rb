require 'test/unit'
require 'hex_generator'

class HexCotentGeneratorTest < Test::Unit::TestCase
  def test_hills
    results = HexContentGenerator.hex_generator("hills", "frontier", 1000)
    assert_equal results.keys.count, 1000
  end

  def test_special
    results = HexContentGenerator.generate_special("plains", "frontier")
  end

  def test_forest
    results = HexContentGenerator.hex_generator("forest", "unsettled", 1000)
    assert_equal results.keys.count, 1000
  end
end